# recipe-app-api-proxy

NGINX proxy app for recipe app api

## Usage

### Environment variables

* 'LISTEN_PORT' - port to listen
* 'APP_HOST' - Host name of app to forward request default to 'app'
* 'APP_PORT' -  Port of the app dafault :'9000'